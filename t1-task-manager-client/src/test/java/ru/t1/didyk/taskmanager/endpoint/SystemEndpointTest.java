package ru.t1.didyk.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.didyk.taskmanager.api.endpoint.ISystemEndpoint;
import ru.t1.didyk.taskmanager.dto.request.ServerAboutRequest;
import ru.t1.didyk.taskmanager.dto.request.ServerVersionRequest;
import ru.t1.didyk.taskmanager.dto.response.ServerAboutResponse;
import ru.t1.didyk.taskmanager.dto.response.ServerVersionResponse;
import ru.t1.didyk.taskmanager.marker.IntegrationCategory;
import ru.t1.didyk.taskmanager.service.PropertyService;

@Category(IntegrationCategory.class)
public class SystemEndpointTest {

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = ISystemEndpoint.newInstance(propertyService);

    @Test
    public void getAboutTest() {
        @NotNull ServerAboutRequest request = new ServerAboutRequest();
        @Nullable final ServerAboutResponse response = systemEndpoint.getAbout(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.getName().isEmpty());
        Assert.assertFalse(response.getEmail().isEmpty());
    }

    @Test
    public void getVersionTest() {
        @NotNull ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        Assert.assertNotNull(response);
        Assert.assertFalse(response.getVersion().isEmpty());
    }

}
