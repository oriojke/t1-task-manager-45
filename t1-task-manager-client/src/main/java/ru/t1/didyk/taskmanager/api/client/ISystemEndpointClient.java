package ru.t1.didyk.taskmanager.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.didyk.taskmanager.dto.request.ServerAboutRequest;
import ru.t1.didyk.taskmanager.dto.request.ServerVersionRequest;
import ru.t1.didyk.taskmanager.dto.response.ServerAboutResponse;
import ru.t1.didyk.taskmanager.dto.response.ServerVersionResponse;

public interface ISystemEndpointClient {

    @NotNull ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
