package ru.t1.didyk.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;

public interface IDatabaseProperty {

    @NotNull String getDatabaseUser();

    @NotNull String getDatabasePassword();

    @NotNull String getDatabaseUrl();

    @NotNull String getDatabaseDriver();

    @NotNull String getDatabaseDialect();

    @NotNull String getHBM2DDL();

    @NotNull String getShowSql();

    @NotNull
    public String getDatabaseUseSecondLvlCache();

    @NotNull
    public String getDatabaseUseQueryCache();

    @NotNull
    public String getDatabaseUseMinimalPuts();

    @NotNull
    public String getDatabaseRegionPrefix();

    @NotNull
    public String getDatabaseCacheProviderConfigFile();

    @NotNull
    public String getDatabaseRegionFactoryClass();

}
