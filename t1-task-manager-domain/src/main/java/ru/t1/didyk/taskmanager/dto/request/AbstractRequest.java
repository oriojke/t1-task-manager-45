package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
public abstract class AbstractRequest implements Serializable {
}
