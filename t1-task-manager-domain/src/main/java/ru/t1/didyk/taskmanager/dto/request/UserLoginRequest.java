package ru.t1.didyk.taskmanager.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserLoginRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    public UserLoginRequest(@Nullable String token) {
        super(token);
    }
}
