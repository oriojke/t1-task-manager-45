package ru.t1.didyk.taskmanager.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public class UserLogoutRequest extends AbstractUserRequest {
    public UserLogoutRequest(@Nullable String token) {
        super(token);
    }
}
