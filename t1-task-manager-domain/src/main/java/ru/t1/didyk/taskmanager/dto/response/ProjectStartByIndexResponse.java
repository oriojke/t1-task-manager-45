package ru.t1.didyk.taskmanager.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.didyk.taskmanager.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public class ProjectStartByIndexResponse extends AbstractProjectResponse {
    public ProjectStartByIndexResponse(@Nullable ProjectDTO project) {
        super(project);
    }
}
